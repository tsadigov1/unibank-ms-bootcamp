package az.ingress.microservicebootcamp.controller;

import az.ingress.microservicebootcamp.model.Student;
import az.ingress.microservicebootcamp.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/student")
@Slf4j
@RequiredArgsConstructor
public class StudentController {

    private final StudentRepository studentRepository;

    @PostMapping
    public Student sayHello2(@RequestBody Student student) {
        Student save = studentRepository.save(student);
        return save;

    }


}
