package az.ingress.microservicebootcamp.controller;

import java.util.List;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties("unibank")
@Configuration
@Data
public class Config {

    private String name;
    private List<String> students;
    private Integer count;
}
