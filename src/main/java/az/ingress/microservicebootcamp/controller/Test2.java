package az.ingress.microservicebootcamp.controller;

public interface Test2 {

    boolean canFly();

    String drive();
}
