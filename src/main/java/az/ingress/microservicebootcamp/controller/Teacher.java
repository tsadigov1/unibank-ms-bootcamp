package az.ingress.microservicebootcamp.controller;

import javax.annotation.Priority;
import org.springframework.stereotype.Component;

@Component("hello")
@Priority(1)
public class Teacher implements Test {

    @Override
    public String toString() {
        return "i am a teacher";
    }
}
