package az.ingress.microservicebootcamp.repository;

import az.ingress.microservicebootcamp.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
